---
disable_pages: false
protected_pods:
  - onap_k8s_openlab
stages:
  - lint
  - config
  - infra_install
  - infra_deploy
  - vim
  - apps
  - check
runner:
  tags:
    - docker
  env_vars:
    CHAINED_CI_SRC: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
  docker_proxy:
  image: registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  image_tag: 2.7.13-alpine

gitlab:
  pipeline:
    delay: 15
  base_url: https://gitlab.com
  api_url: https://gitlab.com/api/v4
  private_token: "{{ lookup('env','CI_private_token') }}"

  git_projects:
    config:
      stage: config
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
      api: https://gitlab.com/api/v4/projects/12930999
      branch: "{{ lookup('env','config_branch')|default('master', true) }}"
      path: pod_config

    trigger:
      stage: apps
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        623033316232313032343633663266383430376630663331306662626536303739633733
        643261353264343065313939343232313930626235336235316337350a37393434656430
        363664643430613666383361616666383233643963376361633431646462303261623536
        6363343033613935613432653863303666366335380a3365303064353933383136353036
        346430656365636232313666366332346366386666343438383631343337616133313162
        6233653333396363386336343436
      branch: "{{ lookup('env','CI_BUILD_REF_NAME')|default('master', true) }}"

    os_infra_manager:
      stage: infra_install
      api: https://gitlab.com/api/v4/projects/12931250
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        366335653132383732323530623037653665643564396139653561313466376437323166
        643738353934623233633165316466363634663562613062656333360a62636535663934
        623732373432373631656261383037316461663664336630636566386662323936646633
        3032633230663033333463666266623133386237300a6661313962346130353930313439
        646239613461363634373735643165346362353863363430353739333932316633353331
        3662343530613133363663356537
      branch: "{{ lookup('env','os_infra_branch')|default('master', true) }}"
      get_artifacts: config
      pull_artifacts: "create_servers"
      timeout: 900
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"

    az_infra_manager:
      stage: infra_install
      api: https://gitlab.com/api/v4/projects/14446574
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/az_infra_manager
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        376164343630383334616532383064343733383362663735353364316533336538633534
        663064303562393232626630616530613436323332663533356635370a31636634303266
        343962643865663862396232643334353065663362326336313337366533626339616333
        6137626236653563663331323062363836623362300a3733393364643635633035646232
        643337396435323832393037356461653065383030623332616430663961376236396134
        6433316463646637346336633164
      branch: "{{ lookup('env','az_infra_branch')|default('master', true) }}"
      get_artifacts: config
      pull_artifacts: "create_servers"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"

    build_so:
      stage: infra_install
      api: https://gitlab.com/api/v4/projects/14144730
      url: https://gitlab.com/Orange-OpenSource/lfn/onap/build-so/
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        3636346437336166646233353432643138333531653232343530646261663531626238
        61666663376139336466633139633037613333666261626264386665390a3038633130
        3735633431656534303234643064303937663538366363363462636234616564383130
        6132353533313738363232386438376363636132333966380a62656534386131306432
        6562663532356434353461656162623238356631353430346365343530663132616530
        66363730343965383634653732343738623030
      branch: "{{ lookup('env','build_so_branch')|default('master', true) }}"
      pull_artifacts:
      timeout: 1400
      get_encrypt: true
      get_bin: true
      parameters:
        GERRIT_REVIEW: "{{ lookup('env','GERRIT_REVIEW') }}"
        GERRIT_PATCHSET: "{{ lookup('env','GERRIT_PATCHSET') }}"
        PROJECT: "{{ lookup('env','PROJECT') }}"

    build_clamp:
      stage: infra_install
      api: https://gitlab.com/api/v4/projects/19317846
      url: https://gitlab.com/Orange-OpenSource/lfn/onap/build-clamp/
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        3361646637323833353562316461623937633732333263366532646638356566303064
        62393664623465373033323530386464366539393365393138323861360a3137333961
        6235386234386533643930653466353332613865623331353934666336366438306663
        6539636133356263373939353462336332303666633031320a30636463333132626132
        3036656337666330636438353265633238323361623762323664626638306361646139
        65396665323435653734316333353333383730
      branch: "{{ lookup('env','build_clamp_branch')|default('master', true) }}"
      pull_artifacts:
      timeout: 1400
      get_encrypt: true
      get_bin: true
      parameters:
        GERRIT_REVIEW: "{{ lookup('env','GERRIT_REVIEW') }}"
        GERRIT_PATCHSET: "{{ lookup('env','GERRIT_PATCHSET') }}"
        PROJECT: "{{ lookup('env','PROJECT') }}"

    kubespray:
      stage: vim
      api: https://gitlab.com/api/v4/projects/6546033
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/kubespray_automatic_installation
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        343061626339393537646564633136663038333861316162656164373836633538643965
        663661333466623737666439373333366663373733633435303535380a64633332613466
        326165313062313665323836646636626466623132303765376630393063363331626261
        6337316263613734316530653363326662646262340a6234666566323937623633623337
        343134653038346236383863333937386665393831616565633838356462316533626136
        6662626534633062636534623865
      branch: "{{ lookup('env','vim_branch')|default('master', true) }}"
      get_artifacts:
      pull_artifacts: "postconfigure"
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        docker_version: "{{ lookup('env','docker_version') }}"
        kubernetes_release: "{{ lookup('env','kubernetes_release') }}"
        kubespray_version: "{{ lookup('env','kubespray_version') }}"
        helm_version: "{{ lookup('env','helm_version') }}"

    rke:
      stage: vim
      api: https://gitlab.com/api/v4/projects/13256264
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/rke_automatic_installation
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        613463396366343831633333306237656561653962353639373066623261336434356434
        623335356265396631353330346133356663343936653931343335350a31646132396534
        306466323135353736653031386564663537303332626436356535626434626366613534
        3038383834653037646363386137376534356637320a3039363833313234613039363663
        666138313566373364393336666536313562323833336664613130323538366365383839
        6331393866346163336363316634
      branch: "{{ lookup('env','vim_branch')|default('master', true) }}"
      get_artifacts:
      pull_artifacts: "postconfigure"
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        docker_version: "{{ lookup('env','docker_version') }}"
        kubernetes_release: "{{ lookup('env','kubernetes_release') }}"
        kubespray_version: "{{ lookup('env','kubespray_version') }}"
        helm_version: "{{ lookup('env','helm_version') }}"

    rke2:
      stage: vim
      api: https://gitlab.com/api/v4/projects/24578550
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        363332323234363335633034343039306430323262383664386138313964326462326262
        346432633432643230626233316361636639373632303266333861630a33343436326561
        653561376564633630656265363966353236376166383365373230666231313364376239
        6361363663393765373932616663666561643733370a3764643235656439656662653932
        366434366135653233333462366366666636616537636139653430363163653932303231
        6361646430646464333966366566
      branch: "{{ lookup('env','vim_branch')|default('master', true) }}"
      get_artifacts:
      pull_artifacts: "postconfigure"
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        docker_version: "{{ lookup('env','docker_version') }}"
        kubernetes_release: "{{ lookup('env','kubernetes_release') }}"
        kubespray_version: "{{ lookup('env','kubespray_version') }}"
        helm_version: "{{ lookup('env','helm_version') }}"

    aks_infra_manager:
      stage: vim
      api: https://gitlab.com/api/v4/projects/14474841
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/aks_infra_manager
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        6233303034353265663266313737333538646135313733326535303836643361643231
        32336433303161626333313630313634663766386165346366353764320a6262373739
        6235633639303832366337313530633533316632636163313530363431303061393165
        6661303836393465653133613238663764336564386264620a31326630383065396166
        3533623663633538336334666165383962363733303664353838616264636232303733
        31313537623463616637663963333936643666
      branch: "{{ lookup('env','aks_infra_branch')|default('master', true) }}"
      get_artifacts: config
      pull_artifacts: "create_cluster"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"

    devstack:
      stage: vim
      api: https://gitlab.com/api/v4/projects/14474652
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/devstack_automatic_installation
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        376363376439646231313835356532323865666538613730353730386633616339343536
        336364616233613831666238346438663562396264313561303062390a62356630353562
        653638346362306130633439393965633337343635316666356466623031313933333039
        6439353964313063306232346138336664316132320a3537653362363337373032626262
        343863366238306262373832633837343566363863653462336363346239663262666262
        3864353861663263383733333630
      branch: "{{ lookup('env','devstack_branch')|default('master', true) }}"
      get_artifacts: config
      pull_artifacts: "create_devstack"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"


    k3s:
      stage: vim
      api: https://gitlab.com/api/v4/projects/23489332
      url: https://gitlab.com/Orange-OpenSource/lfn/tools/k3s-automatic-installation
      trigger_token: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          31373334366331656539313162303965646364646431653737346266333339653635356239623465
          6337356166633732646633343066343964646136643263330a383736616562366263383364393366
          65666532646333386436323935326661326338323435663863373131356238313934336566333966
          6463323366373665370a373736363137336132663230623963636433303330353631396262616466
          66376235323361376538363730643135366430343533313831616239363034333164
      branch:
        "{{ lookup('env','vim_branch')|default('master', true) }}"
      get_artifacts: infra_deploy
      pull_artifacts: postconfigure
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        docker_version: "{{ lookup('env','docker_version') }}"

    harbor:
      stage: apps
      api: https://gitlab.com/api/v4/projects/22950818
      url: https://gitlab.com/Orange-OpenSource/lfn/tools/harbor_automatic_installation
      trigger_token: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          38306637373636343536343264366232613464326332393133373239373062323833633163323232
          3038663931393863316663643934383633333333323861300a656339363032623832323337666264
          35306636376338383666336566653864373362326261616431303936326238653638613565636433
          3662656666633530610a393964336637363834383735666633666466343135616632626233646238
          38333063303863663432303838393634636661313837393434363734303835313565

      branch:
        "{{ lookup('env','vim_branch')|default('master', true) }}"
      get_artifacts: infra_deploy
      pull_artifacts:
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        docker_version: "{{ lookup('env','docker_version') }}"

    acumos:
      stage: apps
      api: https://gitlab.com/api/v4/projects/13059902
      url: https://gitlab.com/Orange-OpenSource/lfn/acumos/acumos-installer
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        613139323634653366343661386361636431633038663337333531663535393439626232
        393037313932363162363430636331343834393337613665653535380a61616665346263
        376534636236376337626231616231623065636462393933313133653931343963666431
        3530646264373137633832376434353433646437310a3634333035393861663331313264
        646266323466343061373835396439613733393562653130373061383530623765313731
        3663666563656436666532363133
      branch: "{{ lookup('env','acumos_branch')|default('master', true) }}"
      get_artifacts: infra_deploy
      pull_artifacts:
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"

    oom:
      stage: apps
      api: https://gitlab.com/api/v4/projects/6550110
      url: https://gitlab.com/Orange-OpenSource/lfn/onap/onap_oom_automatic_installation/
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        363633373665306138306339646434343366613963393165346661373436633032643430
        326536646361313061663837633137663134306331346439313638390a38623034643463
        626666366366633764373132373634626436666132333031303033653133613464363632
        6363366466396136303232356639623961653637340a3136666438333263636436326463
        616462646239323066316231346131623237646238393361643634366436356639386533
        3632353462663933643835656364
      branch: "{{ lookup('env','oom_deploy_branch')|default('master', true) }}"
      pull_artifacts: postinstallation
      timeout: 1400
      get_encrypt: true
      get_bin: true
      parameters:
        GERRIT_REVIEW: "{{ lookup('env','GERRIT_REVIEW') }}"
        GERRIT_PATCHSET: "{{ lookup('env','GERRIT_PATCHSET') }}"
        OOM_BRANCH: "{{ lookup('env','OOM_BRANCH') }}"
        PROJECT: "{{ lookup('env','PROJECT') }}"
        ANSIBLE_VERBOSE: "{{ lookup('env','ansible_verbose') }}"
        TEST_RESULT_DB_URL: "http://onap.api.testresults.opnfv.fr/api/v1/results"

    stackstorm:
      stage: apps
      api: https://gitlab.com/api/v4/projects/13215720
      url: https://gitlab.com/davidblaisonneau-orange/stackstorm
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        3963656531346666386262326339333138373633353331346665343064623663386230
        39343164353539363562353930373839623362363631653030303635360a3365306462
        3034643262633730663731363762613835376436316631393735363839373465366662
        6136343332306166376634613266653965383565663937640a64386463663236323862
        3137326236376333326639366630323535313161643235613237396131323864323233
        32393435316363356638383162616232643535
      branch: "{{ lookup('env','st2_branch')|default('master', true) }}"
      get_artifacts: infra_deploy
      pull_artifacts:
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"

    xtesting-onap:
      stage: check
      api: https://gitlab.com/api/v4/projects/10614465
      url: https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        376564383532616465343061313138373763333833653463333165313062623262303930
        626531653332333063663134393038623661646430633335393266360a35653732613063
        333338356136656332323337623534663964653234613836336530303564653463613838
        3566306635613566373036356135646364613034660a3037323932396165363334616264
        393938316636316437303261323066326530393363303365623036316463613032343533
        3234633838343731333166616632
      branch: "{{ lookup('env','xtesting-onap_branch')|default('master', true) }}"
      get_artifacts: vim
      pull_artifacts:
      timeout: 600
      get_encrypt: true
      get_bin: true
      parameters:
        GERRIT_REVIEW: "{{ lookup('env','GERRIT_REVIEW') }}"
        GERRIT_PATCHSET: "{{ lookup('env','GERRIT_PATCHSET') }}"
        EXPERIMENTAL: "{{ lookup('env','EXPERIMENTAL') }}"
        ONAP_VERSION: "{{ lookup('env','OOM_BRANCH') }}"
        DEPLOY_SCENARIO: os-nosdn-nofeature-ha
        TEST_RESULT_DB_URL: "http://testresults.opnfv.org/onap/api/v1/results"
        PROJECT: "{{ lookup('env','PROJECT') }}"

    functest: &functest
      stage: check
      api: https://gitlab.com/api/v4/projects/13988217
      url: https://gitlab.com/Orange-OpenSource/lfn/infra/functest
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        336638633161666365353764303732666437613030663535636634353436376631363830
        613331393362613465393761333430333737633864376231393130320a64366336633063
        383532656530396262626235343162356635383933306530613861613931323830336365
        6634653261333832353239633239646161336631350a6639663765623865613066333338
        313338336632653936636239356439316165393962376235353765393635396332663963
        3235313763336635383465356438
      branch: "{{ lookup('env','functest_branch')|default('master', true) }}"
      get_artifacts: vim
      pull_artifacts:
      timeout: 600
      parameters:
        TEST_RESULT_DB_URL: "http://testresults.opnfv.fr/test/api/v1/results"

    functest_k8s:
      <<: *functest
      parameters:
        DEPLOY_SCENARIO: k8s-cedric-ftw

    functest_os:
      <<: *functest
      parameters:
        tiers: os_healthcheck # os_smoke, os_benchmarking, os_vnf
        DEPLOY_SCENARIO: os-nosdn-nofeature-ha
